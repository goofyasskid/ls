import socket
import datetime

from main import HOST, PORT

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0, fileno=None) as sock:
        sock.bind((HOST, PORT))
        sock.listen(10)
        while True:
            conn, addr = sock.accept()
            with conn:
                while True:
                    data = datetime.datetime.now().strftime('%d.%m.%Y %H:%M').encode('utf-8')
                    if not data:
                        break
                    conn.sendall(data.upper()) # Для наглядности в верхнем регистре
                    break

if __name__ == '__main__':
    main()
