import os

HOST = os.environ.get('HOST', '127.0.0.1')
PORT = os.environ.get('PORT', 1303)

def main():
    pass

if __name__ == '__main__':
    main()
