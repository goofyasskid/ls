import socket

from main import HOST, PORT

def main():
    client_host = input()
    if client_host:
        host = client_host
    else:
        host = HOST

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((host, PORT))
        print(sock.recv(1024).decode('utf-8'))

if __name__ == '__main__':
    main()
